import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, RequestOptions, XHRBackend, Http } from '@angular/http';
import { RouterModule } from '@angular/router';

// Components
import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';

// Providers
import { InterceptorService } from './services/interceptor.service';

// Custom Modules
import { BookModule } from './modules/book/book.module';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        BookModule,
        RouterModule.forRoot([
            {
                path: 'welcome', component: WelcomeComponent
            },
            {
                path: '', redirectTo: 'welcome', pathMatch: 'full'
            },
            {
                path: '**', redirectTo: 'welcome', pathMatch: 'full'
            }
        ])
    ],
    declarations: [
        AppComponent,
        WelcomeComponent,
    ],
    providers: [
        {
            provide: Http,
            // provide: InterceptorService,
            useFactory: (backend: XHRBackend, options: RequestOptions) => {
                return new InterceptorService(backend, options);
            },
            deps: [XHRBackend, RequestOptions]
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
