import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'star',
    styles: [require('./star.component.less')],
    templateUrl: './star.component.html'
})

export class StarComponent implements OnChanges {
    @Input() rating: number;
    @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
    starWidth: number;

    ngOnChanges(): void {
        this.starWidth = this.rating * (86 / 5);
    }

    onClick(): void {
        this.ratingClicked.emit(`This rating ${this.rating} was clicked!`);
    }
}
