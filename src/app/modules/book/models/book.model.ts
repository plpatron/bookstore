export interface Book {
    name: string;
    author: string;
    description: string;
    price: number;
    starRating: number;
    bookId: number;
    releaseDate: string;
}
