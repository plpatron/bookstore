import { Pipe, PipeTransform } from '@angular/core';

import { Book } from '../models/book.model';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(books: Book[], filterBy: string): Book[] {
        filterBy = filterBy ? filterBy.toLowerCase() : null;
        return filterBy ? books.filter(( book: Book) => {
            return book.name.toLowerCase().indexOf(filterBy) !== -1;
        }) : books;
    }
}