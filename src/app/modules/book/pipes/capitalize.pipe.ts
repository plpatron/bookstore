import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'capitalize'
})

export class CapitalizePipe implements PipeTransform {

    transform (value: string): string {
        let splitted = value.split(' '),
            capitalized = splitted.map((word) => {
                return word.charAt(0).toUpperCase() + word.slice(1);
            });
        return capitalized.join(' ');
    }

}
