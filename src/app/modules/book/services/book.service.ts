import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

// Rxjs operators
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

// Interfaces
import { Book } from '../models/book.model';

@Injectable()
export class BookService {
    private bookCollectionUrl: string = './api/books.json';

    // constructor(private _http: InterceptorService ) {}
    constructor(private _http: Http) {}

    getBooks(): Observable<Book[]> {
        return this._http.get(this.bookCollectionUrl)
                .map((response: Response) => <Book[]> response.json())
                .do((data => console.log(JSON.stringify(data))))
                .catch(this.handleError);
    }

    getBook(id: number): Observable<Book> {
        return this._http.get(this.bookCollectionUrl)
                .map((response: Response) => <Book[]> response.json())
                .map((books: Book[]) => {
                return books.find((book) => {
                    return book.bookId === id
                });
            });
    }

    private handleError(error: Response)  {
        console.log(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
