import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable()
export class CheckGuard implements CanActivate {

    constructor(private _router: Router) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const id = +route.params.id;
        if (isNaN(id) || id < 1) {
            alert('Invalid book Id');
            // start a new navigation to redirect to list page
            this._router.navigate(['/books']);
            // abort current navigation
            return false;
        };
        return true;
    }
}