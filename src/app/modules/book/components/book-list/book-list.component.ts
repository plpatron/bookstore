// Providers
import { Component, OnInit } from '@angular/core';

// Interfaces
import { Book } from '../../models/book.model';

// Services
import { BookService } from '../../services/book.service';

@Component({
    selector: 'book',
    templateUrl: './book-list.component.html',
    styles: [require('./book-list.component.less')]
})
export class BookListComponent implements OnInit {
    bookFilter: string;
    books: Book[] = [];
    errorMessage: string;

    constructor(private _bookService: BookService) {}

    ngOnInit(): void {
        this._bookService.getBooks()
        .subscribe(
            books => this.books = books,
            error => <any> error
        );
    }

    onRatingClicked(message: string): void {
        console.log('Hey ', message);
    }
}
