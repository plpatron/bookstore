import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router  } from '@angular/router';

import { BookService } from '../../services/book.service';

import { Book } from '../../models/book.model';

@Component({
    templateUrl: './book-detail.component.html',
    styles: [require('./book-detail.component.less')]
})
export class BookDetailComponent implements OnInit {
    book: Book;
    errorMessage: string;

    constructor(private _route: ActivatedRoute, private _router: Router, private _bookService: BookService) {}

    ngOnInit(): void {
        let bookId = +this._route.snapshot.params['id'];
        this._bookService.getBook(bookId)
        .subscribe(
            book => this.book = book,
            error => this.errorMessage = <any>error
        );
    }
}
