import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BookListComponent } from './components/book-list/book-list.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';

// Providers
import { BookService } from './services/book.service';
import { CheckGuard } from './guards/check.guard';

// Pipes
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { JoinPipe } from './pipes/join.pipe';

import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'books', component: BookListComponent },
            {
                path: 'book/:id',
                canActivate: [CheckGuard],
                component: BookDetailComponent
            }
        ])
    ],
    declarations: [
        BookListComponent,
        BookDetailComponent,
        CapitalizePipe,
        FilterPipe,
        JoinPipe
    ],
    providers: [
        BookService,
        CheckGuard
    ]
})
export class BookModule { }