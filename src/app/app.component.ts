import { Component } from '@angular/core';

import '../assets/css/global-styles.less';

@Component({
  selector: 'bookstore',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { }
