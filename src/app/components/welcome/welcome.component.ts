import { Component } from '@angular/core';

@Component({
    selector: 'welcome',
    templateUrl: './welcome.component.html',
    styles: [require('./welcome.component.less')]
})

export class WelcomeComponent {}